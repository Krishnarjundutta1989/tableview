//
//  ViewController.h
//  tableView
//
//  Created by click labs 115 on 9/28/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
<UITableViewDelegate,UITableViewDataSource>


@end

