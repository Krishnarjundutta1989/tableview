//
//  ViewController.m
//  tableView
//
//  Created by click labs 115 on 9/28/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSArray *arrayForCell;
    NSArray *arrayForImage;
    NSMutableArray *arrayForTxtTable;
    
    
    
}
@property (strong, nonatomic) IBOutlet UITextField *txtTable;



@property (strong, nonatomic) IBOutlet UITableView *currentTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnTable;



@end

@implementation ViewController
@synthesize currentTableView;
- (IBAction)txtTableView:(id)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayForCell = @[@"Rohit",@"akshay",@"Prince"];
    arrayForImage = @[@"Apple-Logo-iPhone-6-Wallpapers.jpg",@"Best-iPhone-6-Apple-Wallpaper.jpg",@"Black-Apple-iPhone-6-Wallpaper.jpg",@"iPhone-6-Plus-Wallpaper-Apple-Logo-05.jpg"];
    
    arrayForTxtTable = [NSMutableArray new];
    //currentTableView.reloadData;
    // Do any additional setup after loading the view, typically from a nib.
}


-(NSInteger) numberOfSectionsInTableView: (UITableView *)tableView {
        return 1;
    }

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayForTxtTable.count;
}
-(IBAction)btnPressed{
    
    [arrayForTxtTable addObject:[NSString stringWithFormat:@"%@",_txtTable.text]];
    
    [currentTableView reloadData];
}



-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resuseCurrentTableViewCell" ];
                           //  cell.textLabel.text = arrayForCell [indexPath.row];
   cell.textLabel.text = arrayForTxtTable [indexPath.row];
cell.imageView.image = [UIImage imageNamed:arrayForImage[indexPath.row]];
  
    
    
    //cell.imageView.image = arrayForImage [indexPath.row];
                             return cell;
    
    }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
